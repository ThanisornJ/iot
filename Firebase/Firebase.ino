#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

#define WIFI_SSID       "######"
#define WIFI_PASSWORD   "######"

#define FIREBASE_HOST "iot-preset.firebaseio.com"
#define FIREBASE_KEY "CJ67tJzjP7lYbFo23Ru4n7IlEGVPYs7pDg994RCr"
#define deviceNumber 01
#define deviceName "ESP"
void setup() {
  Serial.begin(115200);
  Serial.println(WiFi.localIP());
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  // Do something
  Firebase.begin(FIREBASE_HOST, FIREBASE_KEY);
  Firebase.setString(deviceNumber + "/name", deviceName);
}

void loop() {
  float rawValue = analogRead(A0);
  float percent = (rawValue/1024)*100;

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& valueObject = jsonBuffer.createObject();
  valueObject["percent"] = percent;
  valueObject["raw"] = rawValue;

  Firebase.push(deviceNumber + "/value", valueObject);
  delay(10000);
}
