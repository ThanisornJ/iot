# README #

Iot Final Project

### Hardware Requirement ###

* ESP8266 module 12E ,or NodeMCU
* Sensor (Photoresistor,Temp,etc)
* Wire
* Relay

### Library ###

* Firebase (https://github.com/googlesamples/firebase-arduino/archive/master.zip)
* MQTT (https://github.com/knolleary/pubsubclient/archive/v2.6.zip)

### Arduino IDE ###

* Install ESP8266 board

### Reference ###

* MQTT (https://ioxhop.github.io/ESPIOX2-Document/mqtt-on-adurino-ide.html)
* Firebase (http://www.akexorcist.com/2016/11/firebase-and-esp8266-with-arduino.html)
* ESP8266 AnalogRead (https://netpie.gitbooks.io/nodemcu-esp8266-on-netpie/content/lab-4.html)
* ESP8266 AT Firmware (http://espressif.com/en/support/download/at)
* ESP8266 Flash Firmware (https://goo.gl/Ckt3nz)
### NodeMCU Structure ###
![alt text](https://i.lnwfile.com/_/i/_raw/1h/01/nk.png)